class Result {
  final int id;
  final String poster_path;

  Result({this.id, this.poster_path});

  factory Result.fromJson(Map<String, dynamic> json) {
    return Result(
      id: json['id'],
      poster_path: json['poster_path'],
    );
  }
}