import 'package:movie_db/entities/Result.dart';


class GeneralResponse {
  final int page;
  final int total_pages;
  final List<Result> results;

  GeneralResponse({this.page, this.total_pages, this.results});

  factory GeneralResponse.fromJson(Map<String, dynamic> json) {
    var list = json['results'] as List;
    List<Result> data = list.map((i) => Result.fromJson(i)).toList();


    return GeneralResponse(
      page: json['page'],
      total_pages: json['total_pages'],
      results: data,
    );
  }
}