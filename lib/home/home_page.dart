import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_db/home/home_bloc.dart';

import '../consts.dart';
import 'home_state.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  final ScrollController _scrollController = ScrollController();
  HomePageBloc _postBloc;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _postBloc = BlocProvider.of<HomePageBloc>(context);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var progressBarVisibility = false;
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    double itemWidth = size.width / 2;

    return BlocBuilder<HomePageBloc, HomeState>(
      builder: (buildContext, state) {
        if (state is InitializedHomeState) {
          return Center(child: CircularProgressIndicator());
        }

        if (state is DataFetchedHomeState) {
          return Stack(
            children: <Widget>[
              GridView.builder(
                padding: EdgeInsets.only(
                  top: 5.0,
                ),
                // EdgeInsets.only
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: itemWidth / itemHeight,
                ),
                controller: _scrollController,
                itemCount: state.data.length,
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (_, index) {
                  return _buildRow(state.data[index].poster_path);
                },
              ),
              Visibility(child: Center(child: CircularProgressIndicator()), visible: state.loading),
            ],
          );
        }
        return null;
      },
    );
  }

  Widget _buildRow(String imageUrl) {
    return Center(
      child: CachedNetworkImage(
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.fill,
            ),
          ),
        ),
        imageUrl: MOVIE_IMAGE_BASE_URL + imageUrl,
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= 200.0) {
      _postBloc.add(LoadMoviesEvent());
    }
  }
}
