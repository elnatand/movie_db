import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:movie_db/data/moor_database.dart';

import '../data/movie_repository.dart';
import '../entities/Result.dart';
import 'home_state.dart';

class HomePageBloc extends Bloc<HomeEvent, HomeState> {
  int _pageNumber = 1;
  List<Result> _movies = List();
  AppDatabase _appDatabase = AppDatabase();

  void dispatchEvent({HomeEvent event}) {
    // _eventController.sink.add(event);
    mapEventToState(event);
  }

  void dispose() {}

  @override
  // TODO: implement initialState
  HomeState get initialState => InitializedHomeState();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is LoadMoviesEvent) {
      List<Movie> movies = await _appDatabase.getMoviesByPage(_pageNumber);
      if (movies.length > 0) {
        for (var movie in movies) {
          var result = Result(id: movie.id, poster_path: movie.posterPath);
          _movies.add(result);
        }
        _pageNumber++;
        yield DataFetchedHomeState(data: _movies);
        return;
      } else {
        yield DataFetchedHomeState(data: _movies, loading: true);
        var generalResponse = await MovieRepository.fetchMovies(_pageNumber);
        var results = generalResponse.results;
        _movies.addAll(results);
        yield DataFetchedHomeState(data: _movies, loading: false);
        await _addMoviesToDB(results);
        _pageNumber++;
        return;
      }
    }
  }

  Future _addMoviesToDB(List<Result> results) {
    for (var result in results) {
      var movie = Movie(
          id: result.id, page: _pageNumber, posterPath: result.poster_path);
      _appDatabase.insertMovie(movie);
    }
    return null;
  }
}
