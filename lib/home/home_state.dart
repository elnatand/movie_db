import 'package:movie_db/entities/Result.dart';

abstract class HomeState {}

class InitializedHomeState extends HomeState {}

class DataFetchedHomeState extends HomeState {
  final List<Result> data;

  final bool loading;

  DataFetchedHomeState({this.data, this.loading = false});

  bool get hasData => data.length > 0;
}

class ErrorHomeState extends HomeState {}

class BusyHomeState extends HomeState {}

abstract class HomeEvent {}

class LoadMoviesEvent extends HomeEvent {}
