import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_db/home/home_bloc.dart';
import 'package:movie_db/home/home_page.dart';

import 'home/home_state.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Infinite Scroll',
      home:  BlocProvider(
        create: (context) => HomePageBloc()..add(LoadMoviesEvent()),
        child: HomePage(),
      )
    );
  }
}
