import '../consts.dart';
import '../entities/GeneralResponse.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MovieRepository {
  static Future<GeneralResponse> fetchMovies(int pageNumber) async {
    var url = BASE_URL + "popular?api_key=" + MOVIE_API_KEY + "&page=" + pageNumber.toString();
    final response = await http.get(url);
    final Map moviesMap = JsonCodec().decode(response.body);
    final movies = GeneralResponse.fromJson(moviesMap);
    if (movies == null) {
      throw Exception("An error occurred");
    }
    return movies;
  }
}