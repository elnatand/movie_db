// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moor_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Movie extends DataClass implements Insertable<Movie> {
  final int id;
  final int page;
  final String posterPath;
  Movie({@required this.id, @required this.page, @required this.posterPath});
  factory Movie.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Movie(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      page: intType.mapFromDatabaseResponse(data['${effectivePrefix}page']),
      posterPath: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}poster_path']),
    );
  }
  factory Movie.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return Movie(
      id: serializer.fromJson<int>(json['id']),
      page: serializer.fromJson<int>(json['page']),
      posterPath: serializer.fromJson<String>(json['posterPath']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'page': serializer.toJson<int>(page),
      'posterPath': serializer.toJson<String>(posterPath),
    };
  }

  @override
  MoviesCompanion createCompanion(bool nullToAbsent) {
    return MoviesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      page: page == null && nullToAbsent ? const Value.absent() : Value(page),
      posterPath: posterPath == null && nullToAbsent
          ? const Value.absent()
          : Value(posterPath),
    );
  }

  Movie copyWith({int id, int page, String posterPath}) => Movie(
        id: id ?? this.id,
        page: page ?? this.page,
        posterPath: posterPath ?? this.posterPath,
      );
  @override
  String toString() {
    return (StringBuffer('Movie(')
          ..write('id: $id, ')
          ..write('page: $page, ')
          ..write('posterPath: $posterPath')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(id.hashCode, $mrjc(page.hashCode, posterPath.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Movie &&
          other.id == this.id &&
          other.page == this.page &&
          other.posterPath == this.posterPath);
}

class MoviesCompanion extends UpdateCompanion<Movie> {
  final Value<int> id;
  final Value<int> page;
  final Value<String> posterPath;
  const MoviesCompanion({
    this.id = const Value.absent(),
    this.page = const Value.absent(),
    this.posterPath = const Value.absent(),
  });
  MoviesCompanion.insert({
    @required int id,
    @required int page,
    @required String posterPath,
  })  : id = Value(id),
        page = Value(page),
        posterPath = Value(posterPath);
  MoviesCompanion copyWith(
      {Value<int> id, Value<int> page, Value<String> posterPath}) {
    return MoviesCompanion(
      id: id ?? this.id,
      page: page ?? this.page,
      posterPath: posterPath ?? this.posterPath,
    );
  }
}

class $MoviesTable extends Movies with TableInfo<$MoviesTable, Movie> {
  final GeneratedDatabase _db;
  final String _alias;
  $MoviesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _pageMeta = const VerificationMeta('page');
  GeneratedIntColumn _page;
  @override
  GeneratedIntColumn get page => _page ??= _constructPage();
  GeneratedIntColumn _constructPage() {
    return GeneratedIntColumn(
      'page',
      $tableName,
      false,
    );
  }

  final VerificationMeta _posterPathMeta = const VerificationMeta('posterPath');
  GeneratedTextColumn _posterPath;
  @override
  GeneratedTextColumn get posterPath => _posterPath ??= _constructPosterPath();
  GeneratedTextColumn _constructPosterPath() {
    return GeneratedTextColumn(
      'poster_path',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, page, posterPath];
  @override
  $MoviesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'movies';
  @override
  final String actualTableName = 'movies';
  @override
  VerificationContext validateIntegrity(MoviesCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.page.present) {
      context.handle(
          _pageMeta, page.isAcceptableValue(d.page.value, _pageMeta));
    } else if (page.isRequired && isInserting) {
      context.missing(_pageMeta);
    }
    if (d.posterPath.present) {
      context.handle(_posterPathMeta,
          posterPath.isAcceptableValue(d.posterPath.value, _posterPathMeta));
    } else if (posterPath.isRequired && isInserting) {
      context.missing(_posterPathMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  Movie map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Movie.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(MoviesCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.page.present) {
      map['page'] = Variable<int, IntType>(d.page.value);
    }
    if (d.posterPath.present) {
      map['poster_path'] = Variable<String, StringType>(d.posterPath.value);
    }
    return map;
  }

  @override
  $MoviesTable createAlias(String alias) {
    return $MoviesTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $MoviesTable _movies;
  $MoviesTable get movies => _movies ??= $MoviesTable(this);
  @override
  List<TableInfo> get allTables => [movies];
}
