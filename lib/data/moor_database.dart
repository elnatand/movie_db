import 'package:moor_flutter/moor_flutter.dart';

part 'moor_database.g.dart';

class Movies extends Table {
  IntColumn get id => integer()();

  IntColumn get page => integer()();

  TextColumn get posterPath => text()();
}

//flutter packages pub run build_runner watch
@UseMoor(tables: [Movies])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: 'db.sqlite', logStatements: true));

  @override
  int get schemaVersion => 1;

  Future<List<Movie>> getAllMovies() => select(movies).get();

  Future<List<Movie>> getMoviesByPage(int pageNumber) {
    return (select(movies)..where((movie) => movie.page.equals(pageNumber)))
        .get();
  }

  Stream<List<Movie>> watchAllMovies() => select(movies).watch();

  Future insertMovie(Movie movie) => into(movies).insert(movie);
}
